# GitLab SiteSpeed Exporter

This is a SiteSpeed plugin which exports specific results into a JSON file that can be parsed by GitLab's [Browser Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html) feature.

A sample JSON output of a run:

`
[{"subject":"/","metrics":[{"name":"Transfer Size (KB)","value":"19.5","desiredSize":"smaller"},{"name":"Speed Index","value":0,"desiredSize":"smaller"},{"name":"Total Score","value":92,"desiredSize":"larger"},{"name":"Requests","value":4,"desiredSize":"smaller"}]}]
`

# Contributing to the GitLab SiteSpeed Exporter

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

# License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.